module.exports = {
	// mode: "production",
	mode: "development",
	entry: {
		index: './src/pages/index.ts',
		perilousPath: './src/pages/perilousPath.ts'
	},
	output: {
		path: __dirname + "/public/js"
	},
	resolve: {
		extensions: [".ts", ".js"]
	},
	module: {
		rules: [
			{ test: /\.ts$/, loader: "ts-loader" }
		]
	},
	devServer: {
		contentBase: __dirname + '/public',
		compress: true,
		port: 8009
	}
}
