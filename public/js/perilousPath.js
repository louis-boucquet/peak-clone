/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/pages/perilousPath.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/Canvas.ts":
/*!***********************!*\
  !*** ./src/Canvas.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nvar __importDefault = (this && this.__importDefault) || function (mod) {\r\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\r\n};\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nvar Coordinate_1 = __importDefault(__webpack_require__(/*! ./Coordinate */ \"./src/Coordinate.ts\"));\r\nfunction getCtx(rootElement, size) {\r\n    if (rootElement === void 0) { rootElement = document.body; }\r\n    if (size === void 0) { size = new Coordinate_1.default(innerWidth, innerHeight); }\r\n    var canvas = document.createElement(\"canvas\");\r\n    canvas.width = size.x;\r\n    canvas.height = size.y;\r\n    canvas.style.width = \"\" + size.x;\r\n    canvas.style.height = \"\" + size.y;\r\n    rootElement.appendChild(canvas);\r\n    return canvas.getContext(\"2d\");\r\n}\r\nexports.getCtx = getCtx;\r\n\n\n//# sourceURL=webpack:///./src/Canvas.ts?");

/***/ }),

/***/ "./src/Coordinate.ts":
/*!***************************!*\
  !*** ./src/Coordinate.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nvar Coordinate = /** @class */ (function () {\r\n    function Coordinate(x, y) {\r\n        this.x = x;\r\n        this.y = y;\r\n    }\r\n    Coordinate.prototype.copy = function () {\r\n        return new Coordinate(this.x, this.y);\r\n    };\r\n    // ADD\r\n    Coordinate.prototype.add = function (other) {\r\n        this.x += other.x;\r\n        this.y += other.y;\r\n    };\r\n    // SUBTRACT\r\n    Coordinate.prototype.subtract = function (other) {\r\n        this.x -= other.x;\r\n        this.y -= other.y;\r\n    };\r\n    // MULTIPLY\r\n    Coordinate.prototype.multiply = function (other) {\r\n        this.x *= other.x;\r\n        this.y *= other.y;\r\n    };\r\n    // DIVIDE\r\n    Coordinate.prototype.divide = function (other) {\r\n        this.x *= other.x;\r\n        this.y *= other.y;\r\n    };\r\n    return Coordinate;\r\n}());\r\nexports.default = Coordinate;\r\n\n\n//# sourceURL=webpack:///./src/Coordinate.ts?");

/***/ }),

/***/ "./src/pages/perilousPath.ts":
/*!***********************************!*\
  !*** ./src/pages/perilousPath.ts ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
eval("\r\nvar __importDefault = (this && this.__importDefault) || function (mod) {\r\n    return (mod && mod.__esModule) ? mod : { \"default\": mod };\r\n};\r\nObject.defineProperty(exports, \"__esModule\", { value: true });\r\nvar Coordinate_1 = __importDefault(__webpack_require__(/*! ../Coordinate */ \"./src/Coordinate.ts\"));\r\nvar Canvas_1 = __webpack_require__(/*! ../Canvas */ \"./src/Canvas.ts\");\r\nvar ctx = Canvas_1.getCtx(document.body, new Coordinate_1.default(800, 800));\r\nctx.fillRect(0, 0, 800, 800);\r\n\n\n//# sourceURL=webpack:///./src/pages/perilousPath.ts?");

/***/ })

/******/ });