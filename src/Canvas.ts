import Coordinate from "./Coordinate";

export function getCtx(rootElement = document.body, size = new Coordinate(innerWidth, innerHeight)): CanvasRenderingContext2D {
	const canvas = document.createElement("canvas");
	canvas.width = size.x;
	canvas.height = size.y;
	canvas.style.width = `${size.x}`;
	canvas.style.height = `${size.y}`;

	rootElement.appendChild(canvas);

	return <CanvasRenderingContext2D>canvas.getContext("2d");
}
