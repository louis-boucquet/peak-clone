import Coordinate from "../Coordinate";
import { getCtx } from "../Canvas";

const ctx = getCtx(document.body, new Coordinate(800, 800));

ctx.fillRect(0, 0, 800, 800);
