export default class Coordinate {
	constructor(
		public x: number,
		public y: number
	) {
	}

	public copy() {
		return new Coordinate(this.x, this.y);
	}

	// ADD

	public add(other: Coordinate) {
		this.x += other.x;
		this.y += other.y;
	}

	// SUBTRACT

	public subtract(other: Coordinate) {
		this.x -= other.x;
		this.y -= other.y;
	}

	// MULTIPLY

	public multiply(other: Coordinate) {
		this.x *= other.x;
		this.y *= other.y;
	}

	// DIVIDE

	public divide(other: Coordinate) {
		this.x *= other.x;
		this.y *= other.y;
	}
}